﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SVG_Editor
{
    [Serializable()]
    class SVG_VertLine : SVG_Proto
    {
        protected int startX;
        protected int startY;
        //protected int height;
        protected int selectionPoint;
        protected int selectionDelta;

        protected Pen editingPen;
        protected Pen writtenPen;

        protected const string SAVE_TEXT = "Vert_Line|x={0},y={1},height={2}";
        protected const string SVG_TEXT = "<line x1='{0}' y1='{1}' x2='{2}' y2='{3}' class='irisui-lib-pipe irisui-lib-pipe-maroon'></line>";



        public SVG_VertLine()
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);

        }

        //public SVG_VertLine(string svgText)
        //{
        //    editingPen = new Pen(Color.Red, 5);
        //    writtenPen = new Pen(Color.Blue, 5);
        //}

        public SVG_VertLine(string loadString)
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);

            LoadShape(loadString);
        }


        public SVG_VertLine(int x,int y,int h)
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);
            startX = x;
            startY = y;
            height=h;
            this.x = startX;
            this.y = startY+height;
        }

        protected Pen getPen()
        {
            if (editStarted || selected)
            {
                return editingPen;
            }
            else
            {
                return writtenPen;
            }
        }

        public override void Edit(int x, int y)
        {
            if (!editStarted)
            {
                startX = x;
                startY = y;
                editStarted = true;
            }

            this.y = y;
            x = startX;
        }

        public override void EditEnded(int x, int y)
        {
            this.x = startX;
            this.y = y;
            //this.y = y;
            height = y - startY;
            editStarted = false;
        }

        public override void Selected(int x1, int y1)
        {
            selectionDelta = y1 - selectionPoint;

            startX = x1;


            startY += selectionDelta;
            y += selectionDelta;

            selectionPoint = y1;
            height = y - startY;
        }

        public override void SelectedEnded()
        {
            Deselect();
        }

        public override void DrawShape(System.Drawing.Graphics g)
        {
            g.DrawLine(getPen(), startX, startY, startX, y);
        }

        public override bool TestSelection(int x1, int y1)
        {
            selected = (y1 > startY && y1 < y) && ((x1 > startX - 8) && (x1 < startX + 8));
            selectionPoint = y1;
            return selected;
        }

        public override string SaveShape()
        {
            return string.Format(SAVE_TEXT, startX, startY, height);
        }

        public override string WriteSVG()
        {
            return string.Format(SVG_TEXT, startX, startY, startX, y);
        }

        public override void LoadShape(string loadString)
        {
            string[] stringArray = loadString.Split(',');
            for (int i = 0; i < stringArray.Length; ++i)
            {
                if (stringArray[i].StartsWith("x="))
                {
                    startX = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("y="))
                {
                    startY = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("height="))
                {
                    y = int.Parse(stringArray[i].Substring(7)) + startY;
                }
            }
        }

        public override void ShiftImageForward()
        {
            return;
        }

        public override void ShiftImageBackward()
        {
            return;
        }

    }
}
