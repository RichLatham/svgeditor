﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVG_Editor
{
    //enum ShapeSelection {Horiz_Line,Vert_Line,Valve };


    public partial class Form1 : Form
    {
        //ShapeSelection shapeMode;
        SVG_Proto currentEditedGraphic = null;
        SVG_Proto currentSelectedGraphic = null;
        protected string currentType = string.Empty;
        public List<SVG_Proto> graphicList = new List<SVG_Proto>();
        bool rulerOn = false;

       SVGHandler svgHandler = null;


        bool selectionMode = false;
        bool editMode = false;

        SVGShapeFactory factory = new SVGShapeFactory();

        public Form1()
        {
            factory.ReadConfigFile("shape_config.txt");
            InitializeComponent();
            MakeShapeMenu();
            //TestObjects();
        }

        private void MakeShapeMenu()
        {
            for (int i = 0; i < factory.ShapeCount; ++i)
            {
                SVG_Proto svgShape = factory.GetShape(i);
                ToolStripMenuItem shapeMenuItem = new ToolStripMenuItem(svgShape.ShapeName);
                shapeMenuItem.Click +=shapeMenuItem_Click;
                shapeToolStripMenuItem.DropDownItems.Add(shapeMenuItem);  
            }
        }

        void shapeMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            currentType = tsmi.Text;
            editMode = true;
            SetEditCursor();
        }

        private void TestObjects()
        {
            SVG_HorizLine line1 = new SVG_HorizLine(100, 100, 200);
            graphicList.Add(line1);
            //currentGraphic = line1;
            pictureBox1.Invalidate();
        }

        private void SetEditCursor()
        {
            this.Cursor = Cursors.Cross;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (editMode)
            {
                lblDebug.Text = "edit and no current edit graphic";
                //make a new current object
                //currentEditedGraphic = getCurrentGraphicFromMenu();
               
                    //lines will be set directly from the menu item
                    currentEditedGraphic = factory.GetShape(currentType);
                //draw current object
                currentEditedGraphic.Edit(e.X, e.Y);
                pictureBox1.Invalidate();
            }

            if (selectionMode)
            {
                if (currentSelectedGraphic != null)
                {
                    currentSelectedGraphic.SelectedEnded();
                    currentSelectedGraphic = null;
                }
                lblDebug.Text = "selected and no current selected graphic";
                testForShapeSelection(e.X, e.Y);
                pictureBox1.Invalidate();
            }

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //mouse down and moved
            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            if (editMode && currentEditedGraphic != null)
            {
                lblDebug.Text = "edit and current edit graphic";
                if (currentEditedGraphic != null) currentEditedGraphic.Edit(e.X, e.Y);
                pictureBox1.Invalidate();
            }

            if (selectionMode && currentSelectedGraphic != null)
            {
                lblDebug.Text = "select and current selected graphic";
                if (currentSelectedGraphic != null) currentSelectedGraphic.Selected(e.X, e.Y);
                pictureBox1.Invalidate();
                //pictureBox1.Refresh();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (editMode)
            {
                if (currentEditedGraphic != null)
                {
                    currentEditedGraphic.EditEnded(e.X, e.Y);
                    graphicList.Add(currentEditedGraphic);

                    //FIX:need to set a new graphic object at this point
                    //currentEditedGraphic = null;
                }
                pictureBox1.Invalidate();
            }

            if(selectionMode)
            {
                if (currentSelectedGraphic != null)
                {
                    currentSelectedGraphic.Selected(e.X, e.Y);
                    //currentSelectedGraphic.SelectedEnded(e.X, e.Y);
                    //currentSelectedGraphic.Deselect();
                    //currentSelectedGraphic = null;
                }
                pictureBox1.Invalidate();
            }

            lblDebug.Text = "mouse up";
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (rulerOn) DrawRuler(e.Graphics);

            if(currentEditedGraphic!=null)currentEditedGraphic.DrawShape(e.Graphics);
            
            for (int i = 0; i < graphicList.Count; ++i)
            {
                if (graphicList[i] != null)
                {
                        graphicList[i].DrawShape(e.Graphics);
                }
            }

            //if(currentSelectedGraphic!=null) currentSelectedGraphic.DrawShape(e.Graphics);
        }

        private void selectToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        public void DrawRuler(Graphics g)
        {
            int rows = pictureBox1.Height / 10;
            int cols = pictureBox1.Width / 10;
            Pen rulerPen=new Pen(Color.Red,1);

            for (int i = 0; i < rows; ++i)
            {
                g.DrawLine(rulerPen, 0, i * 10, pictureBox1.Width, i * 10);
            }

            for (int i = 0; i < cols; ++i)
            {
                g.DrawLine(rulerPen,i*10, 0, i*10, pictureBox1.Height);
            }

        }

        private void testForShapeSelection(int x, int y)
        {
            //test for selection and then sort out z-order
            List<SVG_Proto> selectedShapes = new List<SVG_Proto>();

            for (int i = 0; i < graphicList.Count; ++i)
            {
                if (graphicList[i] == null) continue;

                if (graphicList[i].TestSelection(x, y))
                {
                    selectedShapes.Add(graphicList[i]);
                }
            }

            if (selectedShapes.Count == 0) return;
            selectionMode = true;
            currentSelectedGraphic= selectedShapes[0];
            txtBxPointName.Text = currentSelectedGraphic.TagName;   

            /*
            for (int j = 0; j < graphicList.Count; ++j)
            {
                if (graphicList[j] == currentEditedGraphic)
                {
                    graphicList.Remove(currentEditedGraphic);
                }
            }
            */
        }

        //private SVG_Proto getCurrentGraphicFromMenu()
        //{
        //    switch (shapeMode)
        //    {
        //        case ShapeSelection.Horiz_Line:
        //            currentEditedGraphic=new SVG_HorizLine();
        //            break;
        //        case ShapeSelection.Vert_Line:
        //            currentEditedGraphic = new SVG_VertLine();
        //            break;
        //        case ShapeSelection.Valve:
        //            currentEditedGraphic=new SVG_Shape();
        //            break;
        //        default:
        //            break;
        //    }

        //    return currentEditedGraphic;
        //}

        private void horiz_lineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentType = "horiz_line";
            //shapeMode = ShapeSelection.Horiz_Line;
            selectionMode = false;
            editMode = true;
            SetEditCursor();
        }

        private void valveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //shapeMode = ShapeSelection.Valve;
            selectionMode = false;
            editMode = true;
            SetEditCursor();
        }

        private void verticalLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentType = "vert_line";
            //currentEditedGraphic = new SVG_VertLine();
            ///shapeMode = ShapeSelection.Vert_Line;
            selectionMode = false;
            editMode = true;
            SetEditCursor();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //TODO: get this to work on a specific keypress
            if (currentSelectedGraphic != null)
            {
                currentSelectedGraphic.ShiftImageForward();
            }
        }

        private void selectToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            editMode = false;
            selectionMode = true;
            this.Cursor = Cursors.Hand;
            txtBxPointName.Text = "";
            forwardToolStripMenuItem.Enabled = true;
            backwardToolStripMenuItem.Enabled = true;
        }

        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editMode = false;
            selectionMode = false;
            DeselectAllShapes();
            this.Cursor = Cursors.Arrow;
            pictureBox1.Invalidate();
            forwardToolStripMenuItem.Enabled = false;
            backwardToolStripMenuItem.Enabled = false;
        }

        protected void DeselectAllShapes()
        {
            for (int i = 0; i < graphicList.Count; ++i)
            {
                graphicList[i].Deselect();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
       
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                FileHandling fh = new FileHandling();
                graphicList=fh.Load(openFileDialog.FileName);
                editMode = false;
                selectionMode = false;
                pictureBox1.Invalidate();
            }
        }

        private void saveAsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                FileHandling fh = new FileHandling();
                fh.SaveAs(saveFileDialog.FileName, graphicList);
            }
        }

        private void setSVGFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog svgDialog = new SaveFileDialog();

            if (svgDialog.ShowDialog(this) == DialogResult.OK)
            {
                svgHandler = new SVGHandler(svgDialog.FileName);
                linkLabel1.Text = svgDialog.FileName;
                selectionMode = false;
                editMode = false;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void updateSVGFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (svgHandler != null)
            {
                svgHandler.makeSVG(graphicList);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnTagName_Click(object sender, EventArgs e)
        {
            if (currentSelectedGraphic != null)
            {
                currentSelectedGraphic.TagName = txtBxPointName.Text;
            }
        }


        private void forwardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentSelectedGraphic != null)
            {
                currentSelectedGraphic.ShiftImageForward();
                pictureBox1.Invalidate();
            }
        }

        private void backwardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentSelectedGraphic != null)
            {
                currentSelectedGraphic.ShiftImageBackward();
                pictureBox1.Invalidate();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentSelectedGraphic != null)
            {
                        graphicList.Remove(currentSelectedGraphic);
                        currentSelectedGraphic = null;
                        pictureBox1.Invalidate();
            }
        }



        private void rulerOnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rulerOn = !rulerOn;

            if (rulerOn)
            {
                rulerOnToolStripMenuItem.Text = "Ruler Off";
            }

            if (!rulerOn)
            {
                rulerOnToolStripMenuItem.Text = "Ruler On";
            }

            //pictureBox1.Invalidate();
            pictureBox1.Refresh();
        }

        private void menuStrip1_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Console.Write(e.ToString());
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
        }

        private void forwardToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if(currentSelectedGraphic!=null)currentSelectedGraphic.ShiftImageForward();
            pictureBox1.Invalidate();
        }

        private void reverseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(currentSelectedGraphic!=null)currentSelectedGraphic.ShiftImageBackward();
            pictureBox1.Invalidate();
        }
    }
}
