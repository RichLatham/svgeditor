﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SVG_Editor
{
    [Serializable()]
    public class SVG_Shape : SVG_Proto
    {

        protected const string SAVE_TEXT = "{4}|x={0},y={1},tagname={2},facet={3}";

        //protected const string SVG_TEXT = @"<g id='{2}' class='iris-pup-A_Rcycl_Vlv'><use id='iris-tag-{2}' class='iris-tm-valveImg:svgHref' xlink:href='library.svg#blueValveH' x='{0}' y='{1}' /></g>";

        protected Image editImage;
        protected Image selectedImage;

        protected int xDelta;
        protected int yDelta;

        /// <summary>
        /// An array of images that are used for the device in different directions
        /// </summary>
        protected List<SVG_Facet> editFacets = new List<SVG_Facet>();
        protected List<SVG_Facet> selectedFacets = new List<SVG_Facet>();

        /// <summary>
        /// Will point to the index of images to use for this instance
        /// </summary>
        protected int currentImage = 0;

        public SVG_Shape()
        {
        }

        public SVG_Shape(string loadString)
        {
            LoadShape(loadString);
        }

        public override void LoadShape(string loadString)
        {
            string[] stringArray = loadString.Split(',');
            for (int i = 0; i < stringArray.Length; ++i)
            {
                if (stringArray[i].StartsWith("x="))
                {
                    x = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("y="))
                {
                    y = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("tagname="))
                {
                    tagName = stringArray[i].Substring(8);
                }

                if (stringArray[i].StartsWith("facet="))
                {
                    currentImage = int.Parse(stringArray[i].Substring(6));
                }
            }
        }

        public override void Edit(int x, int y)
        {
            if (!editStarted) editStarted = true;
                this.x = x - width / 2;
                this.y = y - height / 2;
       
        }

        public override void EditEnded(int x, int y)
        {
            Edit(x, y);
           editStarted = false;
        }


        public override bool TestSelection(int x1, int y1)
        {
            selected= (x1 >= x && x1 <= (x + width) ) && (y1 >= y && y1 <=   (y + height) );
            xDelta = x1 - x;
            yDelta = y1 - y; 
            return selected;
        }

        public override void Selected(int x, int y)
        {
            this.x = x - xDelta;
            this.y = y - yDelta; ;
        }

        public override void SelectedEnded()
        {
            Deselect();
        }

        public override string SaveShape()
        {
            return string.Format(SAVE_TEXT,x,y,tagName,currentImage,name);
        }

        public override string WriteSVG()
        {
            int svgX = x + (width / 2);
            int svgY = y + (height / 2);
            return string.Format(editFacets[currentImage].svgText, svgX, svgY, tagName);
        }


        public override void DrawShape(Graphics g)
        {

            if (!selected)
            {

                g.DrawImage(editFacets[currentImage].image, x, y);
            }
            else
            {
                g.DrawImage(selectedFacets[currentImage].image, x, y);
            }
        }

        public void AddFacet(string fileName, string svgText)
        {
            if (fileName.EndsWith("_selected"))
            {
                selectedFacets.Add(new SVG_Facet(fileName, ""));
            }
            else
            {
                editFacets.Add(new SVG_Facet(fileName, svgText));
            }

            CalculateImageDimensions();
        }

        public override void ShiftImageForward()
        {
            currentImage = currentImage + 1 > editFacets.Count -1 ? 0 : ++currentImage;
            CalculateImageDimensions();
        }

        public override void ShiftImageBackward()
        {
            currentImage = currentImage - 1 < 0 ? editFacets.Count - 1: --currentImage;
            CalculateImageDimensions();
        }

        protected void CalculateImageDimensions()
        {
            width = editFacets[currentImage].image.Width;
            height = editFacets[currentImage].image.Height;
        }

    }

    public struct SVG_Facet
    {
        public Image image;
        public string svgText;

        public SVG_Facet(string iFile, string sText)
        {
            iFile += ".png";
            //TODO:Add check so that if file does not exist then some default image is shown instead
            image = Image.FromFile(iFile);
            svgText = sText;
        }
    }
}
