﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SVG_Editor
{
    [Serializable()]
    public class SVG_HorizLine : SVG_Proto
    {
        protected int startX;
        protected int startY;
        //protected int width;
        protected int selectionPoint;
        protected int selectionDelta;

        protected Pen editingPen;
        protected Pen writtenPen;

        protected const string SAVE_TEXT = "Horiz_Line|x={0},y={1},width={2}";
        protected const string SVG_TEXT="<line x1='{0}' y1='{1}' x2='{2}' y2='{1}' class='irisui-lib-pipe irisui-lib-pipe-maroon'></line>";


        public SVG_HorizLine()
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);
        }

        //public SVG_HorizLine(string svgText)
        //{
        //    editingPen = new Pen(Color.Red, 5);
        //    writtenPen = new Pen(Color.Blue, 5);
        //}

        public SVG_HorizLine(string loadString)
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);

            LoadShape(loadString);
        }


        public SVG_HorizLine(int x,int y,int w)
        {
            editingPen = new Pen(Color.Red, 5);
            writtenPen = new Pen(Color.Blue, 5);
            startX = x;
            startY = y;
            width = w;
            this.x = startX + w;
            this.y = startY;
        }

        protected Pen getPen()
        {
            if (editStarted || selected)
            {
                return editingPen;
            }
            else
            {
                return writtenPen;
            }
        }

        public override void Edit(int x, int y)
        {
            if (!editStarted)
            {
                startX = x;
                startY = y;
                editStarted = true;
            }

            this.x = x;
            y = startY;
        }

        public override void EditEnded(int x, int y)
        {
            this.x = x;
            this.y = startY;
            //this.y = y;
            width = x - startX;
            editStarted = false;
        }

        public override void DrawShape(Graphics g)
        {
            g.DrawLine(getPen(), startX, startY, x, startY);
        }



        public override bool TestSelection(int x1, int y1)
        {
            selected = (x1 > startX && x1 < x) && ( (y1 > startY-8) && (y1<startY+8));
            selectionPoint = x1;
            return selected;
        }



        public override void Selected(int x1, int y1)
        {
            selectionDelta = x1 - selectionPoint;
            startY = y1;
            startX+=selectionDelta;
            x+=selectionDelta;
            width = x - startX;
            selectionPoint = x1;
        }

        public override void SelectedEnded()
        {
            Deselect();
        }

        public override string SaveShape()
        {
            return string.Format(SAVE_TEXT, startX, startY, x-startX);
        }

        public override string WriteSVG()
        {
            return string.Format(SVG_TEXT, startX, startY, x, startY);
        }

        public override void LoadShape(string loadString)
        {
            string[] stringArray = loadString.Split(',');
            for (int i = 0; i < stringArray.Length; ++i)
            {
                if (stringArray[i].StartsWith("x="))
                {
                    startX = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("y="))
                {
                    y = startY = int.Parse(stringArray[i].Substring(2));
                }

                if (stringArray[i].StartsWith("width="))
                {
                    x = int.Parse(stringArray[i].Substring(6)) + startX;
                }
            }
        }

        public override void ShiftImageForward()
        {
            return;
        }

        public override void ShiftImageBackward()
        {
            return;
        }

    }
}
