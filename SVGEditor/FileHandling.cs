﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SVG_Editor
{
    public class FileHandling
    {
        protected SVGShapeFactory factory = new SVGShapeFactory();

        public FileHandling() 
        {
            factory.ReadConfigFile("shape_config.txt");
        }

        public void SaveAs(string path, List<SVG_Proto> shapes)
        {
            TextWriter saveAsStream = new StreamWriter(path,false,Encoding.UTF8,1024);

            for (int i = 0; i < shapes.Count; ++i)
            {
                saveAsStream.WriteLine(shapes[i].SaveShape());
            }

            saveAsStream.Flush();
            saveAsStream.Close();
        }

        public void Save(string path, List<SVG_Proto> shapes)
        {
            if(File.Exists(path))
            {
            //Stream saveAsStream = File.Create(path);
            //XmlSerializer serializer = new XmlSerializer(shapes.GetType());
            //serializer.Serialize(saveAsStream, shapes);
            //saveAsStream.Close();
            }
            else
            {
                SaveAs(path,shapes);
            }
        }

        public List<SVG_Proto> Load(string path)
        {
            List<SVG_Proto> loadedShapes = new List<SVG_Proto>();

            if (File.Exists(path))
            {
                TextReader loadStream = new StreamReader(path, Encoding.UTF8);
                string svgType = string.Empty;
                string currentLine=string.Empty;
                string[] parts;

                while ((currentLine=loadStream.ReadLine()) != null)
                {
                    parts=currentLine.Split('|');

                    SVG_Proto shape = factory.GetShape(parts[0]);
                    shape.LoadShape(parts[1]);
                    loadedShapes.Add(shape);
                    //switch (parts[0])
                    //{
                    //    case "Valve":
                    //        loadedShapes.Add(new SVG_Shape(parts[1]));
                    //        break;
                    //    case "SVG_HorizLine":
                    //        loadedShapes.Add(new SVG_HorizLine(parts[1]));
                    //        break;
                    //    case "SVG_VertLine":
                    //        loadedShapes.Add(new SVG_VertLine(parts[1]));
                    //        break;
                    //    default:
                    //        break;
                    //}
                }

                loadStream.Close();
            }
            
            return loadedShapes;
        }
    }
}
