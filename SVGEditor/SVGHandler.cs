﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SVG_Editor
{
    public class SVGHandler
    {
        protected string svgPath=string.Empty;

        public SVGHandler(string p)
        {
            svgPath=p;
        }

        public void makeSVG(List<SVG_Proto> shapes)
        {
            TextWriter textWriter = new StreamWriter(svgPath, false, Encoding.UTF8, 1024);
            textWriter.Write(Properties.Resources.SVG_Header);

            for (int i = 0; i < shapes.Count; ++i)
            {
                textWriter.WriteLine();
                textWriter.Write(shapes[i].WriteSVG());
                textWriter.WriteLine();
            }

            textWriter.Write(Properties.Resources.SVG_Footer);
            textWriter.Close();
        }
    }
}
