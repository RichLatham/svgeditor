﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;

namespace SVG_Editor
{
    [Serializable]
    public abstract class SVG_Proto
    {
        protected int x=0;
        protected int y=0;
        protected int z=0;
        protected bool selected=false;
        protected string svgText=string.Empty;
        protected bool editStarted = false;
        protected string name = string.Empty;
        protected int width=0;
        protected int height=0;

        protected string tagName=string.Empty;

        public string TagName
        {
            get { return tagName; }
            set { tagName = value; }
        }

        public string SVGText
        {
            get { return svgText; }
        }

        public string ShapeName
        {
            get { return name; }
            set { name = value; }
        }

        public abstract void Edit(int x,int y);

        public abstract void EditEnded(int x, int y);

        public abstract void Selected(int x1, int y1);

        public abstract void SelectedEnded();

        public abstract void LoadShape(string loadString);

        public abstract void DrawShape(Graphics g);

        public abstract bool TestSelection(int x1, int y1);

        public abstract string WriteSVG();

        public abstract string SaveShape();

        public abstract void ShiftImageForward();

        public abstract void ShiftImageBackward();

        public void Deselect()
        {
            selected = false;
        }

    }


}

    