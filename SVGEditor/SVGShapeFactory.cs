﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SVG_Editor
{
    /*
     * 
     */
 public class SVGShapeFactory
    {
        protected List<SVG_ShapeBuilder> shapes = new List<SVG_ShapeBuilder>();
        protected SVG_ShapeBuilder currentShape = null;

        public int ShapeCount
        {
            get { return shapes.Count; }
        }

        //public SVG_Proto GetShape(int element)
        //{
        //    return shapes[element];
        //}

        public SVG_Proto GetShape(string type)
        {
            if (type.ToLower() == "vert_line") return new SVG_VertLine();
            if (type.ToLower() == "horiz_line") return new SVG_HorizLine();

            for (int i = 0; i < shapes.Count; ++i)
            {
                if (shapes[i].Name.ToLower() == type.ToLower())
                {
                    SVG_Shape svgShape = new SVG_Shape();
                    svgShape.ShapeName = shapes[i].Name;
                    List<SvgTuple> facetsInfo = shapes[i].facetList;
                    for (int j = 0; j < facetsInfo.Count; ++j)
                    {
                        svgShape.AddFacet(facetsInfo[j].file, facetsInfo[j].svgText);
                    }
                    return svgShape;
                }
            }

            return null;
        }

        public SVG_Proto GetShape(int i)
        {
            SVG_ShapeBuilder builder = shapes[i];
            SVG_Shape svgShape = new SVG_Shape();
            svgShape.ShapeName = shapes[i].Name;
            List<SvgTuple> facetsInfo = shapes[i].facetList;
            for (int j = 0; j < facetsInfo.Count; ++j)
            {
                svgShape.AddFacet(facetsInfo[j].file, facetsInfo[j].svgText);
            }
            return svgShape;
        }

        public void ReadConfigFile(string fileName)
        {
            string[] splitString;
            bool foundNewSection = false;
            TextReader txtReader = new StreamReader(fileName);
            string line = string.Empty;

            while ((line = txtReader.ReadLine()) != null)
            {
                Console.WriteLine(line);
                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    currentShape = new SVG_ShapeBuilder();
                    currentShape.Name = line.Substring(1, line.Length - 2);
                    foundNewSection = true;
                }


                if (foundNewSection)
                {
                    splitString = line.Split('|');
                    if (splitString.Length == 2)
                    {
                        currentShape.AddFacet(splitString[0], splitString[1]);
                    }

                    if (line == string.Empty)
                    {
                        shapes.Add(currentShape);
                        foundNewSection = false;
                    }
                }
            }

            txtReader.Close();
 
        }
       }

 /**
  * Contains information to build a new SVG_Shape object
  */
 public class SVG_ShapeBuilder
 {
     public string name = string.Empty;
     public List<SvgTuple> facetList = new List<SvgTuple>();

     public string Name
     {
         get { return name; }
         set { name = value; }
     }


     public void AddFacet(string file, string text)
     {
         facetList.Add(new SvgTuple(file,text));
     }

    
 }

 public class SvgTuple
 {
     public string file = string.Empty;
     public string svgText = string.Empty;

     public SvgTuple(string f, string t)
     {
         file = f;
         svgText = t;
     }
 }
        
    }
